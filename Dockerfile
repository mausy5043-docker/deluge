FROM balenalib/raspberrypi3-debian:latest
LABEL maintainer="Mausy5043"

# Get stuff up and running. Update the repository cache and install 'apt-utils'
# to get rid of the annoying warnings. Also make sure we have 'bash'.
# Install 'tz-data' so we can set the timezone. Then do a full upgrade.
RUN apt-get update \
 && apt-get install apt-utils \
 && apt-get install bash \
                    tzdata \
 && apt-get upgrade

# Set the local timezone
ENV TZ=Europe/Amsterdam
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
 && echo $TZ > /etc/timezone

# Add a local user for 'deluge'
RUN adduser --system -u 1000 deluge

# Install additionally required packages and do some cleaning up
RUN apt-get install -y \
                    geoip-database \
                    gettext \
                    intltool \
                    python \
                    python-chardet \
                    python-libtorrent \
                    python-mako \
                    python-setuptools \
                    python-twisted \
                    python-xdg \
                    xdg-utils \
                    wget \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set the 'deluge' version and archive name
ARG DELUGE_VERSION=1.3.15
ARG TAR_GZ="deluge-${DELUGE_VERSION}.tar.gz"

# Install Deluge
WORKDIR /
COPY ${TAR_GZ} ${TAR_GZ}
RUN tar -zxvf ${TAR_GZ} \
 && rm ${TAR_GZ} \
 && cd deluge-${DELUGE_VERSION} \
 && python setup.py build \
 && python setup.py install \
 && python setup.py install_data

# Expose the deluge control port and the webUI port
EXPOSE 8112
EXPOSE 58846

# Setup volumes
VOLUME /config
VOLUME /data

# Add the start script
ADD start-deluge.sh /start.sh
RUN chmod +x /start.sh

# Get the start.sh script to run on boot
CMD ["/start.sh"]
