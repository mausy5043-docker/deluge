#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DOCKERFILE="./Dockerfile"
DELUGE_VERSION=1.3.15
TAR_GZ="deluge-${DELUGE_VERSION}.tar.gz"

if [ -e ${TAR_GZ} ]; then
  rm ${TAR_GZ}
fi

curl http://download.deluge-torrent.org/source/1.3/${TAR_GZ} -o ${TAR_GZ}

if [ ! -f "${DOCKERFILE}" ]; then
  echo "** File ${DOCKERFILE} not found."
  echo "Execute this script from within ${script_dir}"
  exit 1
fi

# shellcheck disable=SC1090
source "${script_dir}/repo_config.txt"

# build a local image
# shellcheck disable=SC2154
docker build --rm -t "${image}" .

