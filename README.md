# deluge-docker

It is advised to clone the [Git repository](https://gitlab.com/mausy5043-docker/deluge.git) and build your own image from that.


## Installing
The preferred procedure is:
```
git clone https://gitlab.com/mausy5043-docker/deluge.git
cd deluge
./build.sh
./run.sh
```


## Build a fresh image

```
./build.sh
```
This builds the image on and for the Raspberry Pi.


## Run the image

```
./run.sh
```
This runs the container using the image you just built.


## Stop a running container

```
./stop.sh
```
Stops the container and then deletes it. This allows for immediately running a container with the same name without the need to `docker rm` it manually.


## Updating
FIRST!! Make a backup copy of `run.sh` for your own mental health.

```
./update.sh [--all]
```
This force-pulls the current versions of all the files from the remote git repository. Use the `--all` switch to also rebuild the image and restart the container immediately afterwards.
Be aware that this will overwrite/delete any changes you may have made to the files in this folder!

DISCLAIMER:
Use this software at your own risk! We take no responsibility for ANY data loss.
We guarantee no fitness for any use specific or otherwise.
